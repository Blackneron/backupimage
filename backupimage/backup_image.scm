;-----------------------------------------------------------------------
;
;  Filename: backup_image.scm
;
;-----------------------------------------------------------------------
;
;  Actual version: 1.3 (27.05.2018) - Works with GIMP 2.10.2
;
;-----------------------------------------------------------------------
;
;  Changes:
;
;  1.1 - Changes to use xcfgz compressed files.
;  1.2 - Added option to limit the number of files backed up.
;  1.3 - Check if the file was saved as a XCF file.
;
;-----------------------------------------------------------------------
;
;  Description:
;
;  The 'BackupImage' script (Script-Fu) is used with the free & open
;  source image editor GIMP (https://www.gimp.org/) and will create
;  backups of the actual image file while working on it. It will save
;  a backup copy of the actual GIMP image (with the XCF extension) in
;  the same directory as the original under the filename:
;
;      imagename-ext-YYYY-MM-DD-HH-MM.xcfgz
;
;  After installing the script, it will appear in the menu 'Script-Fu'
;  under 'Image Backup'. This script will also work with the new GIMP
;  version of 2.10.2. The original Fu-Script was created in March 2012
;  by Rob Antonishen under the name 'backup_working.scm' and later it
;  was modified by Patrick Biegel under the name 'backup_image.scm'.
;
;-----------------------------------------------------------------------
;
;  Information:
;
;  Script-Fu is a Scheme-based language. A defining description of the
;  algorithmic programming language 'Scheme' can be found under the
;  following link:
;
;      http://schemers.org/Documents/Standards/R5RS/HTML/r5rs.html
;
;-----------------------------------------------------------------------
;
;  License:
;
;  This program is free software and you can redistribute it and/or
;  modify it under the terms of the GNU General Public License as pub-
;  lished by the Free Software Foundation; either version 2 of the
;  License, or (at your option) any later version.
;
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;
;  The GNU Public License is available at
;  http://www.gnu.org/copyleft/gpl.html
;
;-----------------------------------------------------------------------
;
(define (backup-image img inLayer)
  (let*
    (

      ; Calculate the actual timestamp.
      (timestamp (unbreakupstr

        ; Append the year.
        (append (list (number->string (+ 1900 (car (time)))))

          ; Pad to two characters.
          (map (lambda (x) (string-append (make-string (- 2 (string-length (number->string x))) #\0) (number->string x)))

            ; Month is 0 referenced when (time) returns it.
            (append (list  (+ 1  (cadr (time)))) (butlast (cddr (time)))))) "-"))

      ; Initialize the filename.
      (filename "")

      ; Initialize the file extension.
      (extension "")

      ; Initialize the directory name.
      (dirname "")

      ; Duplicate the actual GIMP image.
      (dupimage (car (gimp-image-duplicate img)))

      ; Change the 'maxbackups' value to define the number of backup files to keep!
      (maxbackups 20)

      ; Specify the GIMP extension.
      (gimpextension "xcf")

      ; Initialize the filelist.
      (filelist "")
    )

    ; Helper defines.
    (define split
      (lambda (ls)
        (letrec ((split-h (lambda (ls ls1 ls2)
          (cond
            ((or (null? ls) (null? (cdr ls)))
              (cons (reverse ls2) ls1))
            (else (split-h (cddr ls)
              (cdr ls1) (cons (car ls1) ls2)))))))
          (split-h ls ls '()))))

    (define merge
      (lambda (pred ls1 ls2)
        (cond
          ((null? ls1) ls2)
          ((null? ls2) ls1)
          ((pred (car ls1) (car ls2))
           (cons (car ls1) (merge pred (cdr ls1) ls2)))
          (else (cons (car ls2) (merge pred ls1 (cdr ls2)))))))

    ; pred is the comparison, i.e. <= for an ascending numeric list, or
    ; string<=? for a case sensitive alphabetical sort,
    ; string-ci<=? for a case insensitive alphabetical sort.
    (define merge-sort
      (lambda (pred ls)
        (cond
          ((null? ls) ls)
          ((null? (cdr ls)) ls)
          (else (let ((splits (split ls)))
            (merge pred
              (merge-sort pred (car splits))
              (merge-sort pred (cdr splits))))))))

    (define get-n-items
      (lambda (lst num)
        (if (> num 0)
          (cons (car lst) (get-n-items (cdr lst) (- num 1)))
          '()))) ;'

    (define slice
     (lambda (lst start count)
        (if (> start 1)
          (slice (cdr lst) (- start 1) count)
          (get-n-items lst count))))

    ; Check if the actual image was not be saved before.
    (if (= (string-length (car (gimp-image-get-filename img))) 0)
      (gimp-message "The file must be saved as an '" (string-upcase gimpextension) "' file before a backup can be made!")
      (begin

        ; Get the filename into a variable.
        (set! filename (unbreakupstr (butlast (strbreakup (car (gimp-image-get-name img)) ".")) "."))

        ; Get the extension into a variable.
        (set! extension (car (last (strbreakup (car (gimp-image-get-name img)) "."))))

        ; Get the directory name  into a variable.
        (set! dirname (unbreakupstr (butlast (strbreakup (car (gimp-image-get-filename img)) DIR-SEPARATOR)) DIR-SEPARATOR))

        ; Check if the actual image was not be saved as an XCF file.
        (if (not (equal? (string-downcase extension) gimpextension))
          (gimp-message (string-append "The file must be saved as an '" (string-upcase gimpextension) "' file!"))
          (begin

            ; Update progressbar text.
            (gimp-progress-set-text (string-append "Backup file: " filename "-" extension "-" timestamp ".xcfgz"))

            ; Save a copy of the actual file.
            (gimp-xcf-save 0 dupimage (car (gimp-image-get-active-drawable dupimage))
              (string-append dirname DIR-SEPARATOR filename "-" extension "-" timestamp ".xcfgz")
              (string-append filename "-" extension "-" timestamp ".xcfgz"))

            ; Display an information message.
            (gimp-message (string-append "Backup file: " filename "-" extension "-" timestamp ".xcfgz"))
          )
        )
      )
    )

    ; Clean up the image duplicate.
    (gimp-image-delete dupimage)

    ; Delete extra backups if necessary.
    (when (> maxbackups 0)
      (set! filelist (merge-sort string<=? (cadr (file-glob (string-append dirname DIR-SEPARATOR filename "-" extension "-*.xcfgz") 0))))
      (set! filelist (slice filelist 0 (max (- (length filelist) maxbackups) 0)))
      (map (lambda (x) (file-delete x)) filelist)
    )
  )
)

(script-fu-register "backup-image"
	"<Image>/Script-Fu/Image Backup/Create Backup"
	"Saves a backup copy as [imagename-ext]-YYYY-MM-DD-HH-MM.xcfgz"
	"Rob Antonishen / Patrick Biegel"
	"Rob Antonishen / Patrick Biegel"
	"27.05.2018"
	"*"
	SF-IMAGE    "image"    0
	SF-DRAWABLE "drawable" 0
)
