# BackupImage Script - README #
---

### Overview ###

The **BackupImage** script (Script-Fu) is used with the [**Free & Open Source Image Editor GIMP**](https://www.gimp.org/) and will create backups of the actual image file while working on it. It will save a backup copy of the actual GIMP image (with the XCF extension) in the same directory as the original under the filename **imagename-ext-YYYY-MM-DD-HH-MM.xcfgz**. After installing the script, it will appear in the menu **Script-Fu** under **Image Backup**. This script will also work with the new GIMP version of **2.10.2**. The original Fu-Script was created in March 2012 by **Rob Antonishen** under the name **backup_working.scm** and later it was modified by **Patrick Biegel** under the name **backup_image.scm**.

### Screenshots ###

![BackupImage - Script-Fu Menu](development/readme/backupimage_1.png "BackupImage - Script-Fu Menu")
![BackupImage - Backup Files](development/readme/backupimage_2.png "BackupImage - Backup Files")

### Setup ###

* Start the GIMP image editor.
* In the menu **Edit/Preferences/Folders/Scripts** you can check the path to the GIMP script directory.
* Copy the file **backup_image.scm** into this script directory of your GIMP installation.
* Select **Filters/Script-Fu/Refresh Scripts** to reload the available scripts.
* Open an image in GIMP and save it in the GIMP format **XCF**.
* **Important:** It's necessary to save the image with the extension **XCF** before you can make backups!
* Choose **Script-Fu/Image Backup/Create Backup** to create a backup file of the actual image.
* Check the directory of your image file to see if there was a new backup file created.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **BackupImage** script is licensed under the [**GNU General Public License (GPL)**](https://www.gnu.org/copyleft/gpl.html) which is published on the official site of the [**GNU**](http://www.gnu.org/).
